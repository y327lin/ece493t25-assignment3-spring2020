
import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, env, learning_rate=0.5, reward_decay=0.9, e_greedy=0.05):
        self.actions = range(env.n_actions)
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Expected Sarsa"

    def choose_action(self, observation):
        self.check_state_exist(observation)
 
        # epsilon greedy
        if np.random.uniform() >= self.epsilon:
            state_action = self.q_table.loc[observation, :]
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
            action = np.random.choice(self.actions)
        return action

    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        q = self.q_table.loc[s, a]
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            vals = np.array([self.q_table.loc[s_, ac] for ac in self.actions])
            num_max = np.sum(vals >= vals.max())
            num_ep = len(vals) - num_max
            ex = 0
            for v in vals:
                if v >= vals.max(): ex += (1-self.epsilon)/num_max * v 
                else: ex += self.epsilon/num_ep * v
            q_target = q + self.lr * (r + self.gamma * ex - q)

        else:
            q_target = q + self.lr * (r - q)  # next state is terminal
        self.q_table.loc[s, a] = q_target  # update
        return s_, a_

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )
