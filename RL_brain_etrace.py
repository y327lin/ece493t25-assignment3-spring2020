import numpy as np
import pandas as pd
from maze_env import UNIT, MAZE_W, MAZE_H, origin

class rlalgorithm:
    def build_index(self, env):
        self.walls = [env.canvas.coords(w) for w in env.wallblocks]
        self.pits = [env.canvas.coords(w) for w in env.pitblocks]
        self.state_index = {}
        i = 0
        for x in range(MAZE_W):
            for y in range(MAZE_H):
                center = origin + np.array([x*UNIT, y*UNIT])
                state = [center[0] - 15.0, center[1] - 15.0, center[0] + 15.0, center[1] + 15.0]
                if state not in self.pits and state not in self.walls:
                    self.state_index[str(state)] = i
                    i+=1

    def __init__(self, env, learning_rate=0.1, reward_decay=0.9, e_greedy=0.05, decay=0.5):
        self.build_index(env)
        self.env = env
        self.actions = range(env.n_actions)
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.decay = decay
        self.w = np.zeros(len(self.state_index) * 4, dtype=np.float64)
        self.z = np.zeros(len(self.state_index) * 4, dtype=np.float64)
        self.display_name="Eligibility Trace (Sarsa)"

    def to_features(self, s, a):
        f = np.zeros(np.shape(self.w), dtype=np.float64)
        if s not in self.state_index:
            return f
        else:
            idx = len(self.state_index) * a + self.state_index[s]
            f[idx] = 1
            return f

    def state_action_val(self, s, a):
        f = self.to_features(s, a)
        return np.inner(f, self.w)

    def convert_state(self, s):
        return [float(n.strip()) for n in s[1:-1].split(",")] 

    def choose_action(self, observation):
        if np.random.uniform() >= self.epsilon:
            vals = np.array([self.state_action_val(observation, a) for a in self.actions])
            # print(self.w[self.state_index["[5.0, 85.0, 35.0, 115.0]"]])
            action = np.random.choice(np.flatnonzero(vals == vals.max()))
        else:
            action = np.random.choice(self.actions)
        return action

    def learn(self, s, a, r, s_):
        a_ = self.choose_action(s_)
        tde = r - self.state_action_val(s, a) + self.gamma*self.state_action_val(s_, a_)
        x = self.to_features(s, a)
        self.z = self.gamma*self.decay*self.z + x
        self.w = self.w + self.lr * tde * self.z
        #print(self.lr*tde*self.z[self.state_index[s]])
        if s_ == str(self.env.canvas.coords(self.env.goal)) or s_ in [str(p) for p in self.pits]:
            self.z = np.zeros(np.shape(self.z), dtype=np.float64)
        return s_, a_
