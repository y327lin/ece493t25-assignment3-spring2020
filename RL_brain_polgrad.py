import numpy as np
import pandas as pd
from maze_env import UNIT, MAZE_W, MAZE_H, origin

class rlalgorithm:
    def build_index(self, env):
        self.walls = [env.canvas.coords(w) for w in env.wallblocks]
        self.pits = [env.canvas.coords(w) for w in env.pitblocks]
        self.state_index = {}
        i = 0
        for x in range(MAZE_W):
            for y in range(MAZE_H):
                center = origin + np.array([x*UNIT, y*UNIT])
                state = [center[0] - 15.0, center[1] - 15.0, center[0] + 15.0, center[1] + 15.0]
                if state not in self.pits and state not in self.walls:
                    self.state_index[str(state)] = i
                    i+=1

    def __init__(self, env, learning_rate=0.08, reward_decay=0.9):
        self.build_index(env)
        self.env = env
        self.actions = range(env.n_actions)
        self.lr = learning_rate
        self.gamma = reward_decay
        self.pol = np.full(len(self.state_index) * 4, 0.1, dtype=np.float64)

        self.rewards = []
        self.probs = []
        self.features = []
        self.states = []
        self.display_name="REINFORCE"

    def to_features(self, s, a):
        f = np.zeros(np.shape(self.pol), dtype=np.float64)
        if s not in self.state_index:
            return f
        else:
            idx = len(self.state_index) * a + self.state_index[s]
            f[idx] = 1
            return f

    def preference(self, f):
        return np.inner(f, self.pol)
    
    def prob(self, s):
        p = np.array([self.preference(self.to_features(s, a)) for a in self.actions])
        exp = np.exp(p - np.max(p))
        softmax =  exp / np.sum(exp)
        return softmax

    def choose_action(self, s):
        softmax = self.prob(s)
        rng = np.random.uniform()
        if rng < softmax[0]:
            return 0
        elif rng < softmax[0] + softmax[1]:
            return 1
        elif rng < softmax[0] + softmax[1] + softmax[2]:
            return 2
        else:
            return 3

    def end_update(self):
        rewards = np.array(self.rewards)
        gammas = np.array([self.gamma**i for i in range(len(rewards))])
        
        for ep in range(len(rewards)):
            s = self.states[ep]
            g = np.inner(rewards[ep:], gammas[:len(rewards)-ep])
            dlog = self.features[ep] - sum([self.to_features(s, a)*self.probs[ep][a] for a in self.actions])
            self.pol += self.lr * self.gamma**ep * g * dlog

    def learn(self, s, a, r, s_):
        a_ = self.choose_action(s_)
        x = self.to_features(s, a)
        prob = self.prob(s)
        self.rewards.append(r)
        self.features.append(x)
        self.probs.append(prob)
        self.states.append(s)

        if s_ == str(self.env.canvas.coords(self.env.goal)) or s_ in [str(p) for p in self.pits]:
            self.end_update() 
            self.rewards = []
            self.features = []
            self.probs = []
            self.states = []
        return s_, a_
