import numpy as np
import pandas as pd

class rlalgorithm:

    def __init__(self, env, learning_rate=0.1, reward_decay=0.9, e_greedy=0.05):
        self.actions = range(env.n_actions)
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table1 = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.q_table2 = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Double Q Learning"

    def choose_action(self, observation):
        self.check_state_exist(observation)
 
        # epsilon greedy
        if np.random.uniform() >= self.epsilon:
            vals = np.array([0.0, 0.0, 0.0, 0.0])
            for a in self.actions:
                vals[a] += self.q_table1.loc[observation, a]
                vals[a] += self.q_table2.loc[observation, a]

            action = np.random.choice(np.flatnonzero(vals == vals.max()))
        else:
            action = np.random.choice(self.actions)
        return action

    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if np.random.uniform() > 0.5:
            qtable_upd = self.q_table1
            qtable_max = self.q_table2
        else:
            qtable_upd = self.q_table2
            qtable_max = self.q_table1

        q = qtable_upd.loc[s, a]
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            state_action = qtable_upd.loc[str(s_), :]
            a_max = np.random.choice(state_action[state_action == np.max(state_action)].index)
            q_target = q + self.lr * (r + self.gamma * qtable_max.loc[s_, a_max] - q)
        else:
            q_target = q + self.lr * (r - q)  # next state is terminal
        qtable_upd.loc[s, a] = q_target  # update
        return s_, a_

    def check_state_exist(self, state):
        if state not in self.q_table1.index:
            self.q_table1 = self.q_table1.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table1.columns,
                    name=state,
                )
            )
        if state not in self.q_table2.index:
            self.q_table2 = self.q_table2.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table2.columns,
                    name=state,
                )
            )
